#from Bio import pairwise2
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO, Entrez, Alphabet
import re, sys, os, subprocess
from Bio.Align.Applications import ClustalOmegaCommandline, ClustalwCommandline
import argparse
import requests
import xml.etree.ElementTree as ET
#import lxml.html
import editdistance, random
#import urllib
from Bio.Blast import NCBIWWW, NCBIXML

def checkStopCodons(barcode):
    #print "Checking for stop codons presence"
    nstop_codons = {}


    tables = [5,1,2,3,4,9,10,11,12,13,15,16,21,22,23,24,25,26] #removed  The Alternative Flatworm Mitochondrial Code (table 14) # The Ciliate, Dasycladacean and Hexamita Nuclear Code (table 6)

    check_result = 1
    for frame_n in range(0, 3):
        for ntable in tables:
            #print "Frame" + str(frame_n) + "table# "+ str(ntable)
            seqObject = Seq(barcode[frame_n:], Alphabet.DNAAlphabet())
            seq_translated = str(seqObject.translate(table=ntable))
            stop_codon_idx = [m.start() for m in re.compile('\*').finditer(seq_translated)]
            nstop_codons[frame_n] = {"seq": seq_translated, "nstop": len(stop_codon_idx), "positions": stop_codon_idx}
            min_stopcodons = min([(nstop_codons[k]["nstop"]) for k in nstop_codons.keys()])
            #print "checkStopCodons(df): " + str(min_stopcodons)
            if min_stopcodons == 0:
                print "No stop codons found in frame "+str(frame_n) + " in ntable "+str(ntable)
                #print "checkStopCodons(df): " + str(min_stopcodons) + "  --->  " + seq_translated
                check_result = 0
                return check_result
            #min_stopcodons = min([(nstop_codons[k]["nstop"]) for k in nstop_codons.keys()])
            #print "checkStopCodons(df): " + str(min_stopcodons) +"  --->  "+seq_translated
    return check_result

def getArguments():
    parser = argparse.ArgumentParser(description='FrameShift Auto Corrector')
    parser.add_argument('-i', nargs=1, dest="input_file", default=None, metavar='', type=str, help='Input FASTA File')
    parser.add_argument('-o', nargs=1, dest="output_file", default=None, metavar='', type=str, help='Output FASTA File')


    return parser.parse_args()

def getFastaSeqs(args):
    records = list(SeqIO.parse(args.input_file[0], "fasta"))
    seqs_dict = {}
    for r in records:
        seqs_dict[r.id] = str(r.seq)
    return seqs_dict

def id_generator(size=6, chars=["A","B","C","D","1","2","3","4","5","6","7"]):
    return ''.join(random.choice(chars) for _ in range(size))

def getNCBIseqAndAccession(seq):
    Entrez.email = id_generator()+'@uoguelph.ca'
    print "Executing remote Blast. Submitted query. Waiting ..."
    results_data = NCBIWWW.qblast(program="blastn", database="nt", sequence=seq, megablast=True).read()
    tmp_file_results_blast = "tmp" + id_generator() + ".xml"
    fp = open(tmp_file_results_blast, "w"); fp.write(results_data); fp.close()

    m_pattern = re.compile("Hit");
    result = m_pattern.search(results_data)  # check if blast results have any Hits
    #print results_data, result
    if result:
        fp = open(tmp_file_results_blast, "rU")
        records = NCBIXML.read(fp)
        fp.close()
        os.remove(tmp_file_results_blast)
        ref_seq = re.sub("-","",records.alignments[0].hsps[0].sbjct)
        accession = records.alignments[0].accession+" ("+records.alignments[0].hit_def+" )"
    else:
        print "No BLAST hits ..."
        ref_seq = ""; accession=""

    return ref_seq, accession


def getBOLDSeq(seq):
    # "http://v4.boldsystems.org/index.php/Ids_xml?db=COX1&sequence="+seq
    # http://v4.boldsystems.org/index.php/API_Public/combined?ids=CNRGC847-15
    #get best match for query sequence in BOLD
    url = "http://v4.boldsystems.org/index.php/Ids_xml?db=COX1&sequence=" + seq
    #print "URL: " + url
    try:
        xml_data = requests.get(url, timeout=60); xml_tree_root = ET.fromstring(xml_data.content)
    except StandardError as e:
        print e
        return {}

    xml_dict = {} #{'CNPKH018-14': {'url': 'http://www.boldsystems.org/index.php/Public_RecordView?processid=CNPKH018-14', 'similarity': '1'}, 'CNPPA2125-12': {'url': 'http://www.boldsystems.org/index.php/Public_RecordView?processid=CNPPA2125-12', 'similarity': '0.9985'}

    if len([el for el in xml_tree_root.iter() if el.tag == "match"]) == 0:  # if number of matches is zero
        print "WARNING: Could not get any matches from BOLD for " + seq
        return {}

    for l1 in xml_tree_root:
        for l2 in l1:
            if l2.tag == "ID":
                boldid = l2.text
            #if len(xml_dict) == 0:
                xml_dict[boldid] = {}
            if l2.tag == "similarity":
                xml_dict[boldid]["similarity"] = l2.text
            for l3 in l2:
                if l3.tag == "url":
                    xml_dict[boldid]["url"] = l3.text
        #break  # only top hit is needed, skip the others for now

    #print xml_dict
    #print xml_dict.keys()
    sorted_boldIDs_similarity_tuples = sorted([(k,float(xml_dict[k]["similarity"])) for k in xml_dict.keys()], reverse=True)
    nhits_above_threshold = len([t[1] for t in sorted_boldIDs_similarity_tuples if t[1] >= 0.98])
    #max_similarity = max([tuple[1] for tuple in sorted_boldIDs_similarity_tuples])
    #print sorted_boldIDs_similarity_tuples #('BBHCM546-10', 0.9985), ('AGAKQ2291-17', 1.0)
    #print nhits_above_threshold #95
    #print xml_dict


    if nhits_above_threshold < 3:
        print "INFO: not enough references with similairy > 0.98 (min 3 hits required) "
        return  {}
    else:
        #print "Hits above similairty threshold " + str(nhits_above_threshold)
        #get the sequence lengths for the top 5 sequences

        top_id_seq_lens_dict={}
        print "Getting ref sequences length and selecting the one with max similarity and length..."

        #print sorted_boldIDs_similarity_tuples
        for topid in [sorted_boldIDs_similarity_tuples[i][0] for i in range(0,len(sorted_boldIDs_similarity_tuples)) if sorted_boldIDs_similarity_tuples[i][1] >= 0.98]:
            top_id_seq_lens_dict[topid] = len(getBOLDRefSeqs(topid)["DNA"]) #{'AGAKH211-17': 614, 'CNGBE024-14': 524, 'CNGSF123-15': 573}

        #print top_id_seq_lens_dict
        max_ref_len = max([top_id_seq_lens_dict[key] for key in top_id_seq_lens_dict.keys()])
        top_boldid = [key for key in top_id_seq_lens_dict.keys() if  top_id_seq_lens_dict[key] == max_ref_len][0]
        #print top_id_seq_lens_dict, top_boldid

        #top_boldid = sorted_boldIDs_similarity_tuples[0][0] #the top similarity bold hit (without seq length consideration)

    #print top_boldid


    try:
        url = "http://v4.boldsystems.org/index.php/API_Public/combined?ids=" + top_boldid
        response = requests.get(url)
        tree = ET.fromstring(response.content)
    except:
        return {}

    tags = ["phylum", "class", "family", "order", "genus", "bin_uri", "nucleotides"]
    for elem in tree.iter():
        current_tag = elem.tag
        # print current_tag
        if any([True if t == current_tag  else False for t in tags]):
            for item in elem.iter():
                if item.tag == "name":
                    xml_dict[top_boldid]["bold_tophit_" + current_tag] = item.text
                elif item.tag == "bin_uri":
                    xml_dict[top_boldid]["bold_tophit_" + current_tag] = item.text
                elif item.tag == "nucleotides":
                    xml_dict[top_boldid]["bold_tophit_" + current_tag] = re.sub('-', '', item.text)

    xml_dict[top_boldid]["BOLDID"] = top_boldid
    print xml_dict[top_boldid]

    return xml_dict[top_boldid]


def getBOLDRefSeqs(id):
    url = str("http://v4.boldsystems.org/index.php/Public_RecordView?processid="+id)
    #url = "http://www.uscho.com/rankings/d-i-mens-poll/"
    #print "'"+url+"'"  #DEBUG

    #html_page = urllib.urlopen(url).read()
    #html_page = requests.get(str(url))
    #if len(html_page) == 0:
    #    sys.exit("ERROR: Empty page returned. Can not extract AA seq info ")

    #page = requests.get('http://econpy.pythonanywhere.com/ex/001.html')
    page = requests.get(url, timeout=60)
    page_text = re.sub("\n","",page.text) #remove new lines in the html page


    #tree = lxml.html.fromstring(page.content)

    try:
        search_DNAtext = re.findall('[A|T|C|G|U|W|S|M|K|R|Y|B|D|H|V|N]{100,}', page_text)
        max_len_DNAtext = max([len(t) for t in search_DNAtext])
        DNABOLDseq = [t for t in search_DNAtext if len(t) == max_len_DNAtext][0]

        search_AAtext = re.findall('[A|R|N|D|C|E|Q|G|H|I|L|K|M|F|P|S|T|W|Y|V|X]{5,}', page_text)
        max_len_AAtext = [l for l in sorted([len(t) for t in search_AAtext], reverse=True) if l < max_len_DNAtext][0]
        AABOLDseq = [t for t in search_AAtext if len(t) == max_len_AAtext][0]
        # DNABOLDseq = [re.sub("\n","",elem.text) for elem in (tree.xpath('/html/body/div[1]/div[3]/div/div[2]/div/div/div/div[2]/div[1]/div[5]/div[2]/div/table/tr[5]/td'))[0]][0]
        #AABOLDseq = [re.sub("\n","", elem.text.rstrip()) for elem in (tree.xpath('/html/body/div[1]/div[3]/div/div[2]/div/div/div/div[2]/div[1]/div[5]/div[2]/div/table/tr[7]/td'))[0]][0]

        #remove dashes in sequences if present
        DNABOLDseq = re.sub("-","", DNABOLDseq); AABOLDseq = re.sub("-","", AABOLDseq);
    except:
        DNABOLDseq = ""; AABOLDseq = ""
        pass

    #print DNABOLDseq,AABOLDseq

    return {"DNA": DNABOLDseq, "AA": AABOLDseq}



    #print html_page
    #sys.exit("Checkpoint1")
    #accessionID = re.compile("(nuccore\/)(.*)(\".*)").findall(html_page)[0][1]
    #print accessionID
    #handle = Entrez.efetch(db="nucleotide", id=accessionID, retmode="xml")
    #records = Entrez.read(handle)
    #print len(records)
    #for r in records:
    #    for items in r["GBSeq_feature-table"]:
    #        for item in items["GBFeature_quals"]:
    #            if item["GBQualifier_name"] == "translation":
    #                return item["GBQualifier_value"]


def getTableNumber(ref_dna, ref_protein):
    #print "getTableNumber"


    tables = [1,2,3,4,5,6,9,10,11,12,13,14,15,16,21,22,23,24,25,26]

    distance2table = {}

    for ntable in tables:
        edit_dist = []
        for frame_n in range(0, 3):
            #if len(ref_dna[frame_n:]) % 3 !=0:
            #    print len(ref_dna[frame_n:]), ref_dna[frame_n:]
            seqObject = Seq(ref_dna[frame_n:], Alphabet.DNAAlphabet())
            seq_translated = str(seqObject.translate(table=ntable))
            edit_dist.append(editdistance.eval(seq_translated, ref_protein))
        distance2table[ntable] = min(edit_dist)
    #print distance2table
    #sys.exit("C")
    return min(distance2table,key=distance2table.get)





def writeFasta(seqRecords, path, mode="w"):
    #print seqRecords
    fp = open(path, mode)
    SeqIO.write(seqRecords, fp, "fasta"); fp.close()
    #print "Wrote file to "+path #DEBUG


def getDNAalignment(root_dir):
    #clustalomega_cline = ClustalOmegaCommandline(infile="/Users/kirill/PycharmProjects/MappedReads2Barcode/test.fasta", outfile="/Users/kirill/PycharmProjects/MappedReads2Barcode/test_msa.fasta", verbose=True, seqtype="DNA", threads=4, force=True)
    #clustalomega_cline()

    clustalw_cline = ClustalwCommandline("clustalw2",infile=root_dir+os.sep+sessionID+"tmp.fasta")
    clustalw_cline()
    #print clustalw_cline

    #print(clustalomega_cline)
    alignment={}
    #records = list(SeqIO.parse("/Users/kirill/PycharmProjects/MappedReads2Barcode/test_msa.fasta","fasta"))
    records = list(SeqIO.parse(root_dir+os.sep+sessionID+"tmp.aln", "clustal"))
    #print records
    alignment["query"] = records[0].id
    alignment["aligned_query_seq"] = str(records[0].seq)
    alignment["bold"] = records[1].id
    alignment["aligned_bold_seq"] = str(records[1].seq)
    alignment["length_aligned_bold_seq"] = len(alignment["aligned_bold_seq"].rstrip("-"))
    return alignment
def checkClippingIndices(s_idx,e_idx,seq):
    if s_idx == None or e_idx == None or e_idx < s_idx:
        return 0, len(seq)
    else:
        return s_idx, e_idx

def getAlignOverlapPosition(ref_seq, query_seq, alignment):
    print "get start and end positions of the ref/query overlap to allow correct clipping"
    #find where the ref seq starts
    #find where query seq ends based on regex
    # not very reliable metric
    start_regex_idx = [m.start(0) for m in re.finditer(ref_seq[0:10],  query_seq)]
    end_regex_idx =  [m.end(0) for m in re.finditer(ref_seq[-10:], query_seq)]

    if len(start_regex_idx) > 0:
        start_regex_idx = start_regex_idx[0]
    else:
        start_regex_idx = None
    if len(end_regex_idx) > 0:
        end_regex_idx = end_regex_idx[0]
    else:
        end_regex_idx = None
    if all([m > 0 for m in [start_regex_idx,end_regex_idx]]):
        print "Method 1 start_regex_idx " + str(start_regex_idx)
        print "Method 1 end_regex_idx " + str(end_regex_idx)
        start_regex_idx, end_regex_idx = checkClippingIndices(start_regex_idx,end_regex_idx,query_seq)
        return start_regex_idx, end_regex_idx




    # find where query seq ends based on alignment
    #print alignment

    #CASE 2: search for end clipping position using Alignment
    # nchar_ref_seq = len(ref_seq); nchar_query_seq = len(query_seq)
    # start_align_idx = None; end_align_idx = None
    # if (nchar_query_seq > nchar_ref_seq): #only the cases where query seq is rather larger and can contain chimeras
    #     print re.findall("-{1,}",alignment["aligned_bold_seq"])
    #     ranges_tuples = [(m.start(0), m.end(0), m.end(0)-m.start(0)) for m in re.finditer("-{1,}",alignment["aligned_bold_seq"])]
    #     print ranges_tuples
    #     max_gap_idx =  max(enumerate([ranges_tuples[i][2] for i in range(0, len(ranges_tuples))]))[0] #(1,54) find the longest gap stretch
    #     print max_gap_idx
    #     end_align_idx = ranges_tuples[max_gap_idx][0]
    #     print start_regex_idx, end_align_idx
    #
    #     if(start_regex_idx) != None:
    #         pattern = query_seq[start_regex_idx:start_regex_idx+10]
    #         try:
    #             start_align_idx = [m.start(0) for m in re.finditer(pattern, alignment["aligned_query_seq"])][0]
    #         except:
    #             start_align_idx = None; pass
    #
    #
    # if all([[end_align_idx, start_align_idx] != None]):
    #     cut_query_seq = re.sub("-{1,}", "", alignment["aligned_query_seq"][start_align_idx:end_align_idx])
    #     try:
    #         indices =  [(m.start(0),m.end(0)) for m in re.finditer(cut_query_seq,query_seq)][0] #coult not work if the reference does not exactly match
    #         if all([i >= 0 for i in indices]):
    #            return indices
    #     except StandardError as e:
    #         print e
    #         pass

    #CASE 3: all failed just cut using start and end of the alignment of the ref seq
    #print ref_seq, alignment["aligned_bold_seq"]
    print "Method 2 clipping via alignment start and end"
    print alignment["aligned_bold_seq"], len(alignment["aligned_bold_seq"])
    print ref_seq, len(ref_seq)

    try:
        start_align_idx = min([m.start(0) for m  in re.finditer(ref_seq[0:10], re.sub("(\w{1,1})(-{1,3})(\w{1,1})", r"\1\3", alignment["aligned_bold_seq"]))])
        end_align_idx = max([m.end(0) for m in re.finditer(ref_seq[-3:], alignment["aligned_bold_seq"])])
    except StandardError as e:
        print e
        start_align_idx=0; end_align_idx=len(query_seq)

    print start_align_idx, end_align_idx
    #cut_query_seq = re.sub("-{1,}", "", alignment["aligned_query_seq"][start_align_idx:end_align_idx])
    #try:
    #    start_align_idx, end_align_idx= [(m.start(0), m.end(0)) for m in re.finditer(cut_query_seq, query_seq)][0]
    #except StandardError as e:
    #    print e
    #    start_align_idx = 0; end_align_idx = len(cut_query_seq)
    #    pass

            #start_align_idx = indices[0]; end_align_idx = indices[1]
    checkClippingIndices(start_align_idx,end_align_idx,query_seq)

    return start_align_idx, end_align_idx
    #if all([i >= 0 for i in indices]):
    #    return indices


        #{'query': 'BIOUG36073-G08',
        # 'aligned_bold_seq': 'AACTTTATATTTTATTTTTTTTACCCCCCCCTTTCATCTAATATTGCTCAAACAGCATGATCCTGCAGGAGGAGGAGATCCAATTTTATACCAACACCT---ATTC------------------------------------------------------',
        # 'aligned_query_seq': 'AACTTTATATTTGGATTTGGAAATTAATATTGCTCACGGAGGAAGATCAGTAGATTTCATTATTATTACTTCTTCTGCAGGAGGAGGAGATCCAATTTTATACCAACA',
        # 'bold': 'Ref', 'length_aligned_bold_seq': 661}




def getPolyNDeletionPositions(alignment): #wrt to query seq
    query_aln = alignment["aligned_query_seq"] #aligned
    ref_aln = alignment["aligned_bold_seq"]

    #find polyT ranges
    # FIND positions of REPEATS in query sequence
    find_idxs = re.compile('T{3,}|C{3,}').finditer(ref_aln)
    idxs_ranges_ref_polyTC= [[i.start(), i.end()] for i in find_idxs ]  # positions of polynucleotide repeats (poly dN)
    # print "polyTC ranges "+str(idxs_ranges_ref_polyTC) #DEBUG


    idxs_del_query_aln =  [i.start() for i in re.compile('-{1,1}').finditer(query_aln)]


    #print "Deletion positions in the query sequence  " + str(idxs_del_query_aln) #DEBUG

    # case 1: SINGLE deletion in the overlap region
    start_overlap_pos, end_overlap_pos = getOverlapStartEnd(alignment)  # DEBUG
    if len([i for i in idxs_del_query_aln if i > start_overlap_pos and i < end_overlap_pos]) == 1:
        #delete_position = idxs_del_query_aln[0]
        idxs_del_query_aln = [i for i in idxs_del_query_aln if i > start_overlap_pos and i < end_overlap_pos]
        delete_positions = [idx for idx in idxs_del_query_aln if idx != 0]
        print "Case 1: Single insertion at position " + str(delete_positions)
        return delete_positions, 1  # to continue

    if len([i for i in idxs_del_query_aln if i > start_overlap_pos and i < end_overlap_pos]) == 2:
        print "Case 1B: 2 deletion positions ... "
        idxs_del_query_aln = [i for i in idxs_del_query_aln if i > start_overlap_pos and i < end_overlap_pos]
        return [idx for idx in idxs_del_query_aln if idx !=0], 3

    # case 2: deletion in query at polyT region
    if len(idxs_del_query_aln) > 0:
        for del_pos in  idxs_del_query_aln:
            print "del_pos: "+str(del_pos) #DEBUG
            delete_positions = []
            for i in range(0, len(idxs_ranges_ref_polyTC)):
                if del_pos >= idxs_ranges_ref_polyTC[i][0] and del_pos <= idxs_ranges_ref_polyTC[i][1]:
                    print "Case2: Found qualifying deletion position inside polyTC region at "+str(del_pos) #DEBUG
                    #print "Del position context in query seq "+query_aln[(del_pos-1):(del_pos+2)] #DEBUG
                    delete_positions.append(del_pos)
                delete_positions = [idx for idx in delete_positions if idx != 0]
                return delete_positions, 2 #return that deletion position (only one is allowed). Want to minimize edits
            return [],0
    else:
        return [], 0




def getInsertionPositions(alignment):  #wrt to query seq
    #print "Find insertion positions ..."
    ref_aln = alignment["aligned_bold_seq"]
    #query_aln = alignment["aligned_query_seq"]

    idxs_insert_query_aln = [i.start() for i in re.compile('-{1,1}').finditer(ref_aln)]
    #print idxs_insert_query_aln
    #print "Insertion positions in the query sequence  "+ str(idxs_insert_query_aln) #DEBUG

    #find overlap region
    #print re.compile(re.sub("-","", query_aln)[0:10]).match(ref_aln)

    start_overlap_pos, end_overlap_pos = getOverlapStartEnd(alignment)  # DEBUG

    #print start_overlap_pos, end_overlap_pos  #DEBUG


    #case 1A: find if reference sequence has only a SINGLE insertion in the overlap region
    #temp  overlap = max ref seq length
    #print len(re.sub("-","",ref_aln))
    #print start_overlap_pos, end_overlap_pos, idxs_insert_query_aln
    #print [i for i in idxs_insert_query_aln if i > start_overlap_pos and i < end_overlap_pos]
    if len([i for i in idxs_insert_query_aln if i > start_overlap_pos and i < end_overlap_pos]) == 1:
        #insert_position = idxs_insert_query_aln
        idxs_insert_query_aln = [i for i in idxs_insert_query_aln if i > start_overlap_pos and i < end_overlap_pos]
        insert_positions = [idx for idx in idxs_insert_query_aln if idx != 0]
        print "Case 1: Single insertion at position "+str(insert_positions) #DEBUG
        return insert_positions , 1

    #case 1B: 2 insertion positions and not more
    if len([i for i in idxs_insert_query_aln if i > start_overlap_pos and i < end_overlap_pos]) == 2:
        print "Case 1B: 2 insertion positions ... "
        idxs_insert_query_aln = [i for i in idxs_insert_query_aln if i > start_overlap_pos and i < end_overlap_pos]
        return [idx for idx in idxs_insert_query_aln if idx !=0], 3


    #case 2: insertion in polyN region
    # FIND positions of REPEATS in query sequence
    find_idxs = re.compile('T{3,}|C{3,}').finditer(ref_aln)
    idxs_ranges_ref_polyTC = [[i.start(), i.end()] for i in find_idxs]
    #print "PolyTC ranges in reference " + str(idxs_ranges_ref_polyTC) #DEBUG


    if len(idxs_insert_query_aln) > 0:
            for insert_position in idxs_insert_query_aln:
                print "ins_pos: "+str(insert_position) #DEBUG
                for i in range(0, len(idxs_ranges_ref_polyTC)):
                    insert_positions=[]
                    if insert_position >= idxs_ranges_ref_polyTC[i][0] and insert_position <= idxs_ranges_ref_polyTC[i][1]:
                        print "Case 2: Found qualifying insertion position " + str(insert_position)  # DEBUG
                        #print "Case 2: Insert position context in query seq " + query_aln[(insert_position - 1):(insert_position + 2)]  # DEBUG
                        insert_positions.append(insert_position)
                    insert_positions = [idx for idx in insert_positions if idx != 0]
                    return insert_positions, 2  # return that deletion position (only one is allowed). Want to minimize edits
                return [], 0
    else:
        return [], 0

def getOverlapStartEnd(alignment):
    ref_aln = alignment["aligned_bold_seq"]; ref_seq = re.sub("-","",ref_aln)
    query_aln = alignment["aligned_query_seq"]; query_seq = re.sub("-","",query_aln)

    try:
        start_overlap_ref = re.compile(ref_seq[0:10]).search(ref_aln).span()[0]
        end_overlap_ref = re.compile(ref_seq[-10:]).search(ref_aln).span()[1]
    except:
        start_overlap_ref = 0; end_overlap_ref = len(ref_seq)
        pass

    try:
        start_overlap_query = re.compile(query_seq[0:10]).search(query_aln).span()[0]
        end_overlap_query = re.compile(query_seq[-10:]).search(query_aln).span()[1]
    except:
        start_overlap_query = 0; end_overlap_query = len(query_seq)
        pass

    #print start_overlap_ref,end_overlap_ref,start_overlap_query,end_overlap_query
    return max(start_overlap_ref,start_overlap_query), min(end_overlap_ref,end_overlap_query)


def sequencePatcherDeletions(aln_query_seq,del_positions):
    #print "B:"+aln_query_seq
    aln_query_seq = list(aln_query_seq);
    for del_position in del_positions:
        if del_position is not [0,len(aln_query_seq)]:
            aln_query_seq[del_position] = "N"
    patched_seq = "".join(aln_query_seq)
    #print "A:" + patched_seq
    return patched_seq

def sequencePatcherInsertions(aln_query_seq, insert_positions):
    aln_query_seq = list(aln_query_seq)
    #print len(aln_query_seq), aln_query_seq
    aln_query_seq = [aln_query_seq[i] for i in range(0,len(aln_query_seq)) if i not in insert_positions]
    #print len(aln_query_seq), aln_query_seq
    patched_seq = "".join(aln_query_seq)
    #print len(patched_seq)
    return patched_seq


if __name__ == "__main__":
    global args
    args = getArguments()
    try:
        FNULL = open(os.devnull, 'w')
        stdout = subprocess.call(["clustalw2", "-HELP"],stdout=FNULL)
    except StandardError as e:
        print e
        print "ERROR: Check that clustalw2 is installed on your system. Otherwise go to http://www.clustal.org/clustal2/#Download "

    global sessionID
    sessionID = ''.join(random.choice(["A","B","C","D","E","1","2","3","4","5"]) for _ in range(6)) #uniqueID allowing to run multiple program instances

    try:
        root_dir=re.compile('(.*\[\/|\\\]{1,}.*.\[\/|\\\])(.*)').match(args.output_file[0]).group(2)+os.sep
    except:
        root_dir = os.getcwd()+os.sep
        pass

    #out_root_name = re.sub("[\|/]..+", "", args.output_file[0])
    out_root_name = re.findall("[\w+|\.]+",args.output_file[0])[-1]
    out_root_name = out_root_name.rsplit(".")[0]


    #creat files
    if not os.path.exists(root_dir +sessionID+"_"+ out_root_name+"_successful_alignments"):
        os.mkdir(root_dir + sessionID +"_"+ out_root_name+"_successful_alignments")  # create folder if does not exists



    open(root_dir+sessionID+"_"+out_root_name+"_problematic_alignments.fasta", "w").close()
    out_fasta_dir_recovered = sessionID+"_OK_"+out_root_name+".fasta"; fp=open(out_fasta_dir_recovered,"w"); fp.close()
    out_fasta_dir_NOT_recovered =  sessionID+"_FAILED_"+out_root_name+".fasta"; fp=open(out_fasta_dir_NOT_recovered,"w"); fp.close()
    fp_out_stats = open(root_dir+sessionID+"_"+out_root_name+"_recovery_stats.txt","w")
    fp_out_stats.write("SeqID\tAlnQuerySeq\tBOLDID_NCBIAccession\tAlnRefSeq\tSeqRecovered(Yes/No)?\tDelCase\tDelPosition\tDelOriginalState\tDelFinalState\tInsertCaseNumber\tInsertPosition\tInsertOriginalState\tInsertFinalState\tNameEditOperations\tQueryLen\tRefLen\n");

    fp_out_stats.close()
    #out_fasta_dir = "/Users/kirill/PycharmProjects/FrameShiftMutationCorrector/test_recovered_seqs.fasta"; fp = open(out_fasta_dir, "w"); fp.close()

    input_seqs = getFastaSeqs(args)
    input_n_seqs = len(input_seqs.keys())
    #print input_seqs

    count_recovered_seqs = 0; count_general=0
    #print input_seqs.keys()[3]
    for query_sid in input_seqs.keys():
        #query_sid = "BIOUG36334-F07" #DEBUG #BIOUG33144-D02 - 2  deletions
        print "Processing seq# "+str(count_general+1)+" with SampleID "+query_sid + " out of " +str(input_n_seqs)+ " seqs (session "+sessionID+")"
        count_general += 1

        input_seq = re.sub("-", "", input_seqs[query_sid]) #remove "-" just in case, otherwise might fail to get BOLD reference

        bold_results_dict = getBOLDSeq(input_seq) #query BOLD and get the best reference record
        #{'similarity': '1', 'url': 'http://www.boldsystems.org/index.php/Public_RecordView?processid=XAK128-06',
        # 'BOLDID': 'LILLA167-11', 'bold_tophit_bin_uri': 'BOLD:AAA3889', 'bold_tophit_family': 'Crambidae',
        # 'bold_tophit_genus': 'Parapediasia', 'bold_tophit_class': 'Insecta', 'bold_tophit_phylum': 'Arthropoda',
        # 'bold_tophit_order': 'Lepidoptera',
        # 'bold_tophit_nucleotides': 'AACTTTATATTTTA....'}


        RefSeqs={}; RefSeqs["DNA"]=""; RefSeqs["AA"]=""
        if len(bold_results_dict) == 0: #get bold seq failed
            print "WARNING: No reference found for the SampleID "+query_sid+" ..."
            RefSeqs["DNA"], bold_results_dict['BOLDID'] = getNCBIseqAndAccession(input_seq)
            print  RefSeqs["DNA"], bold_results_dict['BOLDID']

            if len(RefSeqs["DNA"]) == 0: #if NCBI also fails then skip this read
                print "Writting failed sequence to "+ out_fasta_dir_NOT_recovered
                writeFasta(SeqRecord(Seq(input_seq), id=query_sid, description=""), out_fasta_dir_NOT_recovered,"a")  # write sequences that failed recovery
                #print input_seq
                fp_out_stats = open(root_dir+sessionID+"_"+out_root_name+"_recovery_stats.txt", "a")
                fp_out_stats.write(
                    str(query_sid) + "\t" + input_seq + "\t" + "No BOLD reference available" + "\t" +"No BOLD reference available" + "\t" +
                    "No" + "\t" + "None" + "\t" + "None" + "\t" + "None" + "\t" + "None" + "\t"
                    + "None" + "\t" + "None" + "\t" + "None" + "\t"
                    + "None" + "\t" + "None" +"\t"+str(len(input_seq))+"\t"+"None"+"\n")
                fp_out_stats.close()
                continue #skip the rest of the query sequence

        #print "BOLDID: "+ str(bold_results_dict["BOLDID"])
        #bold_ref_seq = bold_results_dict["bold_tophit_nucleotides"]
        #bold_ref_aa_seq = getAABOLDSeq(bold_results_dict["BOLDID"])

        #TO DO: check how many references there are in BOLD database with identity > 99%. If lower than 3 then do not proceed
        if len(RefSeqs["DNA"]) == 0:
            RefSeqs =  getBOLDRefSeqs(bold_results_dict["BOLDID"]) #DEBUG  getBOLDRefSeqs("CNCDS148-13")

        #sys.exit("CP1")

        if len(RefSeqs["DNA"]) == 0:
            print "WARNING: No BOLD sequences retrieved! Could not extract DNABOLDseq and AABOLDSeq for SampleID " + query_sid
            writeFasta(SeqRecord(Seq(input_seq), id=query_sid, description=""), out_fasta_dir_NOT_recovered,"a")  # write sequences that failed recovery
            fp_out_stats = open(root_dir+sessionID+"_"+out_root_name+"_recovery_stats.txt", "a")
            #print input_seq
            fp_out_stats.write(
                str(query_sid) + "\t" + input_seq + "\t" + "No BOLD reference available" + "\t" + "No BOLD reference available" + "\t" +
                "No" + "\t" + "" + "\t" + "" + "\t" + "" + "\t" + "" + "\t"
                + "None" + "\t" + "None" + "\t" + "None" + "\t"
                + "None" + "\t" + "None" +"\t"+str(len(input_seq))+"\t"+"None" "\n")
            fp_out_stats.close()
            continue #skip the rest of the code
        elif (len(RefSeqs["DNA"]) < 650 and bold_results_dict['BOLDID'] < 20) or  (len(re.findall('N{1,1}',RefSeqs["DNA"])) > 5): #query NCBI if RefSeq is too short
            RefSeqs["DNA"], bold_results_dict['BOLDID']  = getNCBIseqAndAccession(input_seq); RefSeqs["AA"] = ""
            print RefSeqs["DNA"], bold_results_dict['BOLDID']

        #print "RefSeqs: "+ str(RefSeqs)
        ntableAlphabet = getTableNumber(RefSeqs["DNA"], RefSeqs["AA"])

        input_seq = input_seqs[query_sid]

        #DEBUG
        #print "Input Query Seq SID " + query_sid
        #print "Input Query Seq " + input_seq
        #print "BOLD REF: " + RefSeqs["DNA"]
        #print "BOLD REF AA: " + RefSeqs["AA"]
        #print "BOLD REF TableN " + str(ntableAlphabet)

        #if RefSeqs == {}:
        #    sys.exit("No BOLD sequences retrieved! Aborting ...")

        #sys.exit("D")
        #print  bold_results_dict
        #print SeqRecord(Seq(input_seq), id=query_sid)
        #write temp fasta for aligner to work on
        writeFasta([SeqRecord(Seq(input_seq), id=query_sid, description=""), SeqRecord(Seq(RefSeqs["DNA"]), id="Ref_"+bold_results_dict['BOLDID'], description="")],  path=root_dir+sessionID+"tmp.fasta")

        #input_seq = "AACATTATACTTTATTTTTGGGGCTTGAGCAGGAATAGTAGGAACTTCTTTAAGAATTTTAATTCGAGGAAAAATAAGAAAACCTGGAGCATTAATTGGTGACGATCAAATGTATAATGTGATTGTAACAGCTCATGCTTTTATTATAATTTTCTTTATAGTTATACCTATTATAATTGGAGGTTTTGGTAATTGATTAGTTCCTTTAATATTAGGAGCATTCTGAATACTACCTCCATCTATTACTCTATTAGTATCTAGTAGTTTAGTTGAAATAGGAGCAGGAACAGTATACCCCCCACTTTCATCTGGAATTGCTCATGCTGGAGCATCTGTAGATTTAGCAATTTTTTCATTACATTTAGCAGGAATCTCTTCTATTTTAGGAGCTATAAATTTTATTACAACTGTAATTAATATACGAGCTTCAGGTATTACTTTTGATCGAATACCTTTATTTGTTTGATCAGAAGTTATTACAGCAGTATTAGTTACTTCTTTCTTTACCTGTATTAGCTGGAGCTATTACTATACTATTAACAGATCGGAATTTAAATACTTCATCTTTTGACCCTGCTGGAGAGGAGATTCTATTCTTTACCAACATTTATTT"
        #checkStopCodons(input_seq)



        # barcode = "TACCTTATATTTAATTTTCGGAGTTTGGGCCGCGATAGTGGGAACTGCCTTCAGAGTTTTAATTCGCTTAGAGTTAGGACAACCTGGTAGATTTATTGGAGATGATCAAATTTACAATGTTATAGTTACTGCACATGCTTTTATTATAATTTTTTTTATAGTTATACCTATTATAATTGGGGGATTTGGAAACTGATTAGTACCTTTAATAATCGGTGCCCCTGATATAGCATTTCCCCGGATAAATAACATAAGATTCTGGCTTCTTCCCCCTCTCTAACCCTTTTATTAACAGGAGGGTTAGTAGAAAGAGGAGCCGGAACTGGATGAACGGTTTACCCCCCTTTAGCTGCGGGTATTGCTCACGCAGGGGCCTCAGTTGACCTCTCAATTTTTAGACTACATTTAGCAGGTGCTTCTTCAATTCTAGGAGCTGTAAATTTTATTACAACTATTATTAATATACGAACACCTGGCATATCCTGGGATCAAACACCTTTATTTGTTTGATCAGTTTTTTTAACAGCCATTTTACTTCTTCTTTCTTTACCTGTTTTAGCTGGTGCTATTACCATATTATTGACAGATCGAAATCTTAATACGTCTTTTTTTGACCCTGCAGGAGGTGGGGACCCAATTTTATACCAACACTTATTT"
        # bold_seq = "TACATTATATTTTATATTTGGAGCATGAGCTGGAATAGTAGGAACTTCATTAAGAATTTTAATTCGATCAGAATTAGGTCATCCAGGATCATTAATTGGAAATGATCAAATTTATAATGTTATTGTTACTGCTCATGCATTTGTAATAATTTTCTTTATAGTAATACCAATAATAATTGGAGGATTTGGTAATTGATTAGTTCCATTAATATTAGGTTCACCTGATATAGCTTTTCCTCGTATAAATAATATAAGTTTTTGATTATTACCTCCTTCACTTACACTTTTATTAACTAGAAGAATAGTAGAAAATGGATCTGGTACAGGNTGAACAGTTTATCCTCCTTTATCTTCAACAATCGCTCATGGAGGAGCATCAGTTGATCTAGCTATTTTTTCTCTTCATTTAGCAGGAATTTCATCAATTTTAGGAGCTGTAAATTTTATTACTACAGTAATTAATATACGATCAATTGGAATTACATTTGATCGNATACCTTTATTTGTTTGATCAGTTGTAATTACAGCTTTACTTTTACTTTTATCATTACCTGTATTAGCAGGAGCAATNACTATATTATTAACTGATCGTAATTTAAATACTTCATTTTTTGATCCAGCAGGAGGAGGAGATCCTATTTTATATCAACATTTATTT"
        # barcode = "AACATTATATTTTATTTTTGGTATATGATCAGGATTTATCGGAACTTCATTAAGGATAATTTTACGAATTGAATTAAGATCTGTAAGAAATTTAATTGGAAATGATCAAATTTATAATGTTGTTGTTACTGCTCATGCTTTTATTATAATTCTTTTTATAGTTATACCTGTATTAATTGGAGGATTTGGAAATTGAATGATTCCTTTAATATTAGGAGCTCCCGACATAGCATTTCCTCGCTTAAATAATATGAGTTTTTGATTTCTTCCCCCGGCTCTTACATTGCTCTTATTAAGAATGATTATAGAATCTGGAACTGGGACTGGATGAACTGTTTACCCCCCCTCTTTCAGCCAATATTTCTCATTCAGGTATTTCTGTAGATTTGTCAATTTTTAGACTTCATATGGCAGGAATTTCATCAATTTTAGGGGCGATTAATTTTATTTCAACCGTATTTAATATAAAAGTTACTAAAATAAAACTTGATTATTTGACATTATTTATTTGATCTGTTTTAATTACTACAGTTTTATTATTATTGTCTTTACCAGTTTTAGCTGGAGCTATTACAATATTATTAACTGATCGAAATATAAACACGACTTTTTTTGATCCTGCGGGAGGGGGGGATCCAATTTTATATCAACATTTATTT"
        # bold_seq = "ACATTATATTTTATTTTTGGTATATGATCAGGATTTATCGGAACTTCATTAAGGATAATTTTACGAATTGAATTAAGATCTGTAAGAAATTTAATTGGAAATGATCAAATTTATAATGTTGTTGTTACTGCTCATGCTTTTATTATAATTTTTTTTATAGTTATACCTGTATTAATTGGAGGATTTGGAAATTGAATGATTCCTTTAATATTAGGAGCTCCCGATATAGCATTTCCTCGCTTAAATAATATGAGTTTTTGATTTCTTCCCCCGGCTCTTACATTGCTCTTATTAAGAATGATTATAGAATCTGGAACTGGGACTGGATGAACTGTTTACCCCCCTCTTTCAGCCAATATTTCTCATTCAGGTATTTCTGTAGATTTGTCAATTTTTAGACTTCATATGGCAGGAATTTCATCAATTTTAGGGGCGATTAATTTTATTTCAACCGTATTTAATATAAAAGTTACTAAAATAAAACTTGATTATTTGACATTATTTATTTGATCTGTTTTAATTACTACAGTTTTATTATTATTGTCTTTACCAGTTTTAGCTGGAGCTATTACAATATTATTAACTGATCGAAAT"
        # http://v4.boldsystems.org/index.php/Ids_xml?db=COX1&sequence="+seq
        # http://v4.boldsystems.org/index.php/API_Public/combined?ids=CNRGC847-15
        # http://v4.boldsystems.org/index.php/Public_RecordView?processid=CNRGC847-15


        #alignment = pairwise2.align.globalms(barcode, bold_seq,2,-1,-5, -2)[0]

        alignment = getDNAalignment(root_dir)
        #print input_seq, len(input_seq)
        input_seq=re.sub("-","",input_seq)
        #print input_seq, len(input_seq)
        #print [m.start(0) for m in re.finditer("-{10,}",input_seq)]
        #sys.exit()
        #print input_seq, len(input_seq)
        #{'query': 'BIOUG36073-G08', 'aligned_bold_seq': 'AACTTTATATTTTATTTTTGGAA', 'aligned_query_seq': 'AACC',bold': 'Ref', 'length_aligned_bold_seq': 661}
        seq_clip_positions=(None,None)
        #print alignment
        if len(input_seq) > 659:
            seq_clip_positions = getAlignOverlapPosition(re.sub("-","", alignment['aligned_bold_seq']),input_seq,alignment)
        #else:
        #    seq_clip_positions=(0, len(input_seq))


        print "Clipping positions "+str(seq_clip_positions)

        #sys.exit("CP1")


        #identify potential positions where deletion happened next to polyN regions
        del_positions, case_del_id = getPolyNDeletionPositions(alignment)
        print "Deletion position(s): "+str(del_positions) #DEBUG
        # identify potential positions where insertion happened next to polyN regions
        insert_positions, case_ins_id = getInsertionPositions(alignment)
        print insert_positions, case_ins_id
        print "Insert position(s) "+str(insert_positions) #DEBUG
        #if insert_position == None:
        #sys.exit("???")


        #check for insertion and deletion errors in query seq
        DeletionOriginalState = None; DeletionFinalState=None; InsertionOriginalState=None; InsertionFinalState=None
        patched_query_seq = ""
        if del_positions != []:
            patched_query_seq = sequencePatcherDeletions(alignment["aligned_query_seq"], del_positions)
            #remove alignment dashes
            patched_query_seq = re.sub("-","", patched_query_seq)

            DeletionOriginalState = alignment["aligned_query_seq"][del_positions[0]]
            DeletionFinalState = patched_query_seq[del_positions[0]]

        if insert_positions != []:
            if patched_query_seq == "":
                patched_query_seq =  sequencePatcherInsertions(alignment["aligned_query_seq"], insert_positions)
            else:
                patched_query_seq = sequencePatcherInsertions(patched_query_seq, insert_positions)
            print insert_positions
            InsertionOriginalState = alignment["aligned_query_seq"][insert_positions[0]]
            InsertionFinalState = "-"

        # remove "-" symbols
        # perfrom clipping of sequences based on reference
        patched_query_seq = re.sub("-", "", patched_query_seq)
        #print "Patched seq: "+patched_query_seq, len(patched_query_seq); #sys.exit("CHP3")

        #none of the cases worked, do not output such sequence
        if del_positions == [] and insert_positions == []:
            #print "Alignment:"
            #print "Q: " + alignment["aligned_query_seq"]
            #print "R: " + alignment["aligned_bold_seq"]
            #print "WARNING: Not a known case. Could not patch seq of SampleID "+ query_sid #DEBUG
            # write out problematic alignment
            writeFasta([SeqRecord(Seq(alignment["aligned_query_seq"]), id=query_sid, description=""),
                        SeqRecord(Seq(alignment["aligned_bold_seq"]), id="Ref_" + bold_results_dict["BOLDID"], description="")],
                       root_dir + sessionID+"_"+out_root_name+ "_problematic_alignments.fasta", "a")


        if len(patched_query_seq) != 0:
            patched_query_seq = patched_query_seq[seq_clip_positions[0]:seq_clip_positions[1]]
            result_stop_codon = checkStopCodons(patched_query_seq)  # hopefully no stop codons after the correction
        else:
            patched_query_seq = input_seq[seq_clip_positions[0]:seq_clip_positions[1]]
            result_stop_codon = checkStopCodons(patched_query_seq)
        print "Stop codons found "+ str(result_stop_codon)
        #print seq_clip_positions[0], seq_clip_positions[1]
        #print "Patched or clipped seq: " + patched_query_seq +" Len: "+ str(len(patched_query_seq)) # DEBUG

        recovered_flag = "No"
        if  result_stop_codon == 0:
            #print "Recovered "+query_sid
            recovered_flag = "Yes"
            count_recovered_seqs +=1
            #write query sequences out
            writeFasta(SeqRecord(Seq(patched_query_seq), id=query_sid, description=""), out_fasta_dir_recovered,"a")

            #write alignments for possible later inspection (query,reference,corrected_query)
            SeqOutList = [SeqRecord(Seq(patched_query_seq), id=query_sid+"_patched_unaligned", description=""),
                          SeqRecord(Seq(RefSeqs["DNA"]), id=bold_results_dict["BOLDID"] + "_ref_unaligned", description=""),
                          SeqRecord(Seq("*"*500), id="SPACER", description=""),
                          SeqRecord(Seq(alignment["aligned_query_seq"]), id=query_sid+"_original",description=""),
                          SeqRecord(Seq(alignment["aligned_bold_seq"]), id=bold_results_dict["BOLDID"]+"_ref",description="")]

            writeFasta(SeqOutList,root_dir+sessionID+"_"+ out_root_name+"_successful_alignments"+os.sep+sessionID+"_"+query_sid+".fasta" ,"w" ) #sequences that were recovered
            #fp_out = open(out_fasta_dir, "a")
        else:
            print "Stop codons still present :-(. Failed to recover sequence with sampleID "+query_sid
            #write out problematic alignments
            writeFasta([SeqRecord(Seq(alignment["aligned_query_seq"]), id=query_sid, description=""),
                    SeqRecord(Seq(alignment["aligned_bold_seq"]), id="Ref_" + bold_results_dict["BOLDID"], description="")],
                   root_dir +sessionID+"_"+out_root_name + "_problematic_alignments.fasta", "a")
            writeFasta(SeqRecord(Seq(input_seq[seq_clip_positions[0]:seq_clip_positions[1]]), id=query_sid, description=""), out_fasta_dir_NOT_recovered, "a") #write sequences that failed recovery

        #print "write stats on sequences"

        #account for zero base indexing
        #print del_position, insert_position

        # edit operations
        editOperations = []
        if del_positions != []:
            del_positions[0] +=1
            editOperations.append("deletion")
        else:
            del_positions = [None]
        if insert_positions != []:
            insert_positions[0] +=1
            editOperations.append("insertion")
        else:
            insert_positions = [None]

        if len(editOperations) == 0:
            editOperations = ["None"]



        #fp_out_stats.write("SeqID\tQuerySeq\tBOLDID\tReferenceSeq\t SeqRecovered(Yes/No)?
            ## \tDeletionCaseNumber\tDeletionPosition\tDeletionOriginalState\tDeletionFinalState\t
            # #InsertionCaseNumber\tInsertionPosition\tInsertOriginalState\tInsertFinalState\tEditOperations\n");
                            #Seq                  #QuerySeq                           #BOLDID
        fp_out_stats = open(root_dir+sessionID+"_"+out_root_name+"_recovery_stats.txt", "a")
        fp_out_stats.write(str(query_sid)+"\t"+alignment["aligned_query_seq"]+"\t"+bold_results_dict["BOLDID"]+"\t"+alignment["aligned_bold_seq"]+"\t"+
                           recovered_flag+"\t" + str(case_del_id) + "\t" + str(del_positions[0]) + "\t" + str(DeletionOriginalState) + "\t"+str(DeletionFinalState) +"\t"
                           +str(case_ins_id) + "\t" + str(insert_positions[0]) + "\t" + str(InsertionOriginalState)  + "\t"
                           +str(InsertionFinalState)+"\t" + ",".join(editOperations)  + "\t"
                           +str(len(input_seq[seq_clip_positions[0]:seq_clip_positions[1]]))+"\t"+str(len(re.sub("-","",alignment["aligned_bold_seq"])))+"\n")
        fp_out_stats.close()

        if count_general % 10 == 0:
            print "INFO: Processed "+str(count_general)+" recovered " + str(count_recovered_seqs) + " out of " + str(input_n_seqs) + " or completed "+str(count_recovered_seqs/float(input_n_seqs)*100) + "%"


        #sys.exit("CP2")
    print "-"*30
    print "Root directory " + root_dir
    print "Recovery rate is " + str(round(count_recovered_seqs / float(count_general) * 100,0)) + "% ("+str(count_recovered_seqs)+"/"+str(count_general)+")"
    print "Recovered sequences are at "+ sessionID+args.output_file[0]
    print "Recovery stats per each sequence are at "+ root_dir + sessionID+"recovery_stats.txt"


    #series of QC filters
    #if start position is not OK
    #remove_idx_positions = []
    #if alignment["aligned_bold_seq"][0] == "-":
    #    remove_idx_positions.append(0)
        #final_barcode=alignment["aligned_query_seq"][1:]





    # FIND location of DELETIONS in query
    #iter = re.compile("-").finditer(alignment["aligned_query_seq"])
    #del_positions = [i.start() for i in iter if i.start() < alignment["length_aligned_bold_seq"]]  #positions of a gap match

    # FIND location of INSERTIONS in query
    #iter = re.compile("-").finditer(alignment["aligned_bold_seq"])
    #insert_positions = [i.start() for i in iter] # positions of a gap match

    # FIND positions of REPEATS in query sequence
    #iter = re.compile('T{4,}|C{4,}|A{4,}|G{4,}').finditer(alignment["aligned_query_seq"])
    #idx_repeats_ranges_query = [[i.start(), i.end() ] for i in iter]  #positions of polynucleotide repeats (poly dN)

    #
    # # FIND positions of REPEATS in bold ref sequence
    # iter = re.compile('T{4,}|C{4,}|A{4,}|G{4,}').finditer(alignment["aligned_bold_seq"])
    # idx_repeats_bold = [i.start() for i in iter]  # positions of polynucleotide repeats (poly dN)
    #
    # #idx_delta_repeat_positions = [i for i in idx_repeats_query if i not in idx_repeats_bold and i not in insert_positions]
    #
    # #SCENARIO 1 - there is a deletion in query sequence in polydN region range
    # #for p in del_positions:
    # #    new_seq = []
    # #    for i in range(0,len(idx_repeats_ranges_query)):
    # #        if p >= idx_repeats_ranges_query[i][0] and p <= idx_repeats_ranges_query[i][1]:
    #             print p
    #             for i in range(0, len(alignment["aligned_query_seq"])):
    #                 # print any([i == j for j in remove_idx_positions])
    #                 if i == p:
    #                     new_seq.append(alignment["aligned_bold_seq"][i])
    #                 else:
    #                     new_seq.append(alignment["aligned_query_seq"][i])
    #
    #             new_seq = "".join(new_seq)
    #             new_seq = re.sub("-", "", new_seq)
    #             print "new_seq: " + new_seq
    #             checkStopCodons(new_seq,[ntableAlphabet])
    #
    # # first try to add extra character
    #
    # import collections
    # for i in idx_delta_repeat_positions:
    #     print "change position "+str(i)
    #     new_seq = []
    #     sub_seq = alignment["aligned_query_seq"][i:i+5]
    #     base = collections.Counter(sub_seq).most_common(1)[0][0]
    #     for j in range(0, len(alignment["aligned_query_seq"])):
    #         if i is j:
    #             new_seq.append(alignment["aligned_query_seq"][j], base) #try with insert
    #         else:
    #             new_seq.append(alignment["aligned_query_seq"][j])
    #     new_seq = "".join(new_seq)
    #     new_seq = re.sub("-", "", new_seq)
    #     print "Changing base "+base+" at position "+str(i)
    #     print new_seq
    #     result = checkStopCodons(new_seq)
    #     if result == 0:
    #         print "All OK";  print "".join(new_seq); break
    #     elif result == 1:
    #         print "Not OK. Continue searching"
    #
    # #
    #
    #
    # #for inc in range(1,6):
    # #    idx = [i+inc for i in idx_repeats if i+inc in idx_dashes and i+inc < alignment["length_aligned_bold_seq"]]
    # #    if len(idx) == 1:
    # #        remove_idx_positions.append(idx[0])
    #
    # #remove characters at position 342 due to gap present in ref sequence. Also remove character at position 0 again due to gap presence in ref seq
    # new_seq = []
    # #if gap in the seq
    # iter = re.compile("-").finditer(alignment["aligned_query_seq"])
    # change_idx_positions = [i.start() for i in iter]
    # #change_idx_positions = [99,340,404,572,598]
    # #remove clusters of insert positions (sequential chunks)
    #
    #
    # # search for GAPS in the query sequence (e.g. insertions)
    # #if len(change_idx_positions) > 0:
    # #    for i in range(0,len(alignment["aligned_bold_seq"])):
    # #        if any([i == j  for j in change_idx_positions]) == True:
    # #            new_seq.append(alignment["aligned_bold_seq"][i])
    # #            print "Inserted "+alignment["aligned_bold_seq"][i]+" at position "+str(i)
    # #        else:
    # #            new_seq.append(alignment["aligned_query_seq"][i])
    #
    # # search for INSERTIONS in the query sequence (e.g. insertions)
    # if len(change_idx_positions) == 0:
    #     for i in range(0,len(alignment["aligned_query_seq"])):
    #         #print any([i == j for j in remove_idx_positions])
    #         if any([i == j  for j in remove_idx_positions]) == False:
    #             new_seq.append(alignment["aligned_query_seq"][i])
    #         else:
    #             print "Removed "+ alignment["aligned_query_seq"][i] + " at position " +str(i)
    #
    # new_seq = "".join(new_seq)
    # new_seq = re.sub("-","",new_seq)
    #
    # print "new_seq: "+new_seq
    # #new_seq = alignment["aligned_query_seq"][1:342]+alignment["aligned_query_seq"][343:]
    #
    # #new_seq="ACATTATATTTTATTTTTGGTATATGATCAGGATTTATCGGAACTTCATTAAGGATAATTTTACGAATTGAATTAAGATCTGTAAGAAATTTAATTGGAAATGATCAAATTTATAATGTTGTTGTTACTGCTCATGCTTTTATTATAATTCTTTTTATAGTTATACCTGTATTAATTGGAGGATTTGGAAATTGAATGATTCCTTTAATATTAGGAGCTCCCGACATAGCATTTCCTCGCTTAAATAATATGAGTTTTTGATTTCTTCCCCCGGCTCTTACATTGCTCTTATTAAGAATGATTATAGAATCTGGAACTGGGACTGGATGAACTGTTTACCCCCCTCTTTCAGCCAATATTTCTCATTCAGGTATTTCTGTAGATTTGTCAATTTTTAGACTTCATATGGCAGGAATTTCATCAATTTTAGGGGCGATTAATTTTATTTCAACCGTATTTAATATAAAAGTTACTAAAATAAAACTTGATTATTTGACATTATTTATTTGATCTGTTTTAATTACTACAGTTTTATTATTATTGTCTTTACCAGTTTTAGCTGGAGCTATTACAATATTATTAACTGATCGAAATATAAACACGACTTTTTTTGATCCTGCGGGAGGGGGGGATCCAATTTTATATCAACATTTATTT"
    #
    # result = checkStopCodons(new_seq)  #hopefully no stop codons after the correction
    # if result == 0:
    #     print "All OK"
    # elif result == 1:
    #     print "Not OK"
