# FrameShiftCorrector
Correct frame shift errors in the sequences provided "the best match" reference sequence.
To prevent original data alteration, only sequneces requiring up to 2 nucleotide changes are recovered.
The addressed cases are described below in detail. Average recovery rate seen in OPP dataset is up to 63%.
The average speed of processing is limited by the BOLD API querying and is on average 25 sec/sequence. 

### Recovery Cases
The following cases are considered for recovery. The query sequence (`query`) refers to the input sequence with the stop codons.
The reference sequence (`ref`) refers to the one extracted from BOLD

1. DELETION cases (in the query sequence)
    1. CASE 1: single nt deletion in the query sequence in the overlap region with the reference seq. 
    This single deletion **MIGHT BE** outside the polyT or polyC region (but not necessaraly)
    ```
        query   ACC-GTA
        ref     ACCTGTA
    ```
    1. CASE 2: single nt deletion in the query sequence near the polyT or polyC region usually occuring at the 149-157 bp region
    ```
        query   ATTTTTTTTTT-GTA
        ref     ATTTTTTTTTTTGTA
        
        query   CTTCCCCC-TC
        ref     CTTCCCCCCTC 
    ```
    
    
2. INSERTION cases in the query sequence
    1. CASE 1: single nt insertion (in the polyT or polyC regions)
    ```
    query   ACCCCCCCTC
    ref     ACCCCCC-TC
    ```
    1. CASE 2: single nt is insertion that **MIGHT BE** outside of the polyC or polyN region 
    ```
     query  CCCCCGGTCG
     ref    CCCCCN-TCG
    ```
### Sequence clipping
Sometimes sequences are longer than expected amplicon. In this cases the BOLD reference is used to provide information on the 
clipping positions. The output sequences are not only corrected against presence of stop codons, but also are appropriately clipped.

### Reference searches
At least 3 sequences in BOLD with similairty > 0.98 are required for sucessful recovery. Otherwise the query sequence is routed to the
failed set of sequences found in `FAILED_{output_name}.fasta`

### SessionID labelling
Each run is is prefixed with the unique 6 character session ID alphanumerical number. This allows to run the program in the current directory and
avoid name clash overlap or accidental file overwrite and data loss.


### Requirements
1. `clustalw2` is installed and is accessible from any directory (check your $PATH variable). Copy of `clustalw2` for major OS
systems could be downloaded [here](http://www.clustal.org/clustal2/#Download). The latest version is 2.0.12
2.  Python 2.7 installed and available systemwide. Can be downloaded [here](https://www.python.org/downloads/)
3. `BioPython` and `requests`, `lxml` adn `editdistance` libraries installed. Instructions are (here)[http://biopython.org/wiki/Download] 

**Note:** to compile `editdistance` library please install `Microsoft Visual C++ Compiler for Python 2.7` [https://www.microsoft.com/en-us/download/details.aspx?id=44266](https://www.microsoft.com/en-us/download/details.aspx?id=44266) 

```
#on Windows
${full_path2python_install_directory}\Scripts\pip.exe install biopython
pip.exe install biopython
#Successfully installed biopython-1.70 numpy-1.13.3
pip.exe install requests
pip.exe install lxml
pip.exe editdistance

#on Linux, MacOS
pip install biopython
pip install requests
pip install lxml
pip editdistance
```
 
### Get help
To get help run `python FrameShiftMutationCorrector.py -h`


```
python FrameShiftMutationCorrector.py -h
usage: FrameShiftMutationCorrector.py [-h] [-i] [-o]

FrameShift Auto Corrector

optional arguments:
  -h, --help  show this help message and exit
  -i          Input FASTA File
  -o          Output FASTA File

```


### Parameters
The parameters are very simple. You need input seqeunces in the FASTA format. The output is delivered
in FASTA format. **Note:** If these sequences are to be uploaded to BOLD, the title line should contain
only SampleID or ProcessID (e.g. `>BIOUG35735-D08`).

|Parameter         | Name        |Description                                                                           |
|------------------|-------------|-------------------------------------------------------------------------------------|
| -i               | input  FILE |absolute path to the input FASTA file with sequences to recover (stop codons present)|             
| -o               | output FILE |name prefix of all output files (FASTA, report). Files will be deposited in current working directory.     |


### Files
1. `recovery_summary.txt` contains information on all sequences recovery and sequence changes. It contains the following columns
    1. Recovered? is the sequence was successfully recovered
    1. DelCase# the deletion case number (0 meaning that deletion case was not found) (see Recovery Cases section)
    1. DelPos#  nucleotide position in the alignment where deletion was found
    1. InsCase# the insertion case number (0 meaning that insertion case was not found) (see Recovery Cases section)
    1. InsPos#  nucleotide position in the alignment where insertion was found
    1. QuerySeq aligned query sequence
    1. RefSeq   aligned reference sequence
    
1. `problematic_alignments.fasta` clustalw2 aligned sequences in the FASTA format with sequences that could not be recovered but have BOLDID
1. `sucessful_alignments` folder contains aligned and unaligned query and best reference sequences (in FASTA format) for visual inspection
1. `OK_{output_name}.fasta` contains all recovered sequences with no stop codons and optimally clipped
1. `FAILED_{output_name}.fasta` contains all sequences that could not be recovred for whatever reason (stop codons still present, no BOLD reference found)

### Examples and Results
From OPP dataset run 29 (OPP29) found a total of 314 sequences with stop codons. The `FrameShiftCorrect` recovered
200 sequences. 

```
    python FrameShiftMutationCorrector.py -i sequences.fasta -o recovered.fasta
```

The following results were obtained for the OPP dataset. Zero stop codons are found after the run. Note that sequences are not aligned
but codons are colored based on the amino acid they code in the frame #2 using the invertebrate DNA alphabet
![alt text](./test_recovered_seqs.png)
